# 用于管理地址
class HtmlUrl:
    def __init__(self):
        self.new_urls = set()
        self.old_urls = set()

    def add_new_url(self,url):
        self.new_urls.add(url)

    def add_old_url(self,url):
        self.old_urls.add(url)

    def get_new_url(self):
        return self.new_urls.pop()
