# 实体类
class Game:
    def __init__(self, discount, original_price, final_price, name, tag):
        self.discount = discount
        self.original_price = original_price
        self.final_price = final_price
        self.name = name
        self.tag = tag

    def __eq__(self, o: object) -> bool:
        return self.name == o.name

    def __hash__(self) -> int:
        return hash(self.name)

    def __str__(self) -> str:
        return '{discount : %s,original_price : %s,final_price : %s,name : %s,tag : [%s]}' \
               % (self.discount, self.original_price, self.final_price, self.name, self.tag)
