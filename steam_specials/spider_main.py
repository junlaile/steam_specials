# 启动类
# !/usr/bin/python3
from steam.steam_specials.data_storage import DataStorage
from steam.steam_specials.html_downloader import HtmlDownloader
from steam.steam_specials.html_parser import HtmlParser
from steam.steam_specials.html_url import HtmlUrl


class SpiderMain:
    def __init__(self):
        self.html_downloader = HtmlDownloader()
        self.html_parser = HtmlParser()
        self.html_url = HtmlUrl()
        self.data_storage = DataStorage()


if __name__ == '__main__':
    main = SpiderMain()
    main.html_url.add_new_url('https://store.steampowered.com/specials#p=0&tab=TopSellers')
    # main.html_url.add_new_url('https://store.steampowered.com/search/?filter=topsellers&specials=1')
    # for i in range(3):
    #     main.html_url.add_new_url('https://store.steampowered.com/specials#p=' + i.__str__() + '&tab=TopSellers')
    url = main.html_url.get_new_url()
    html = main.html_downloader.downloader(url)
    result = main.html_parser.parser(html)
    main.data_storage.storage(result)
    main.html_url.add_old_url(url)
