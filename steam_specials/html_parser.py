# 数据处理
import re

from steam.steam_specials.game import Game


class HtmlParser:
    item = r'''<a .* class="tab_item  ".*>[\s\S]*?</a>'''
    discount_pct = r'<div class="discount_pct">([\s\S]*?)</div>'
    discount_original_price = r'<div class="discount_original_price">¥ ([\s\S]*?)</div>'
    discount_final_price = r'<div class="discount_final_price">¥ ([\s\S]*?)</div>'
    tab_item_name = r'<div class="tab_item_name">([\s\S]*?)</div>'
    tags = r'<span class="top_tag">([\s\S]*?)</span>'

    def parser(self, html):
        all = re.findall(self.item, html)
        result = set()
        for s in all:
            pct = re.findall(self.discount_pct, s)
            original_price = re.findall(self.discount_original_price, s)
            final_price = re.findall(self.discount_final_price, s)
            name = re.findall(self.tab_item_name, s)
            tag = re.findall(self.tags, s)
            for value in range(len(tag)):
                tag[value]=tag[value].replace(', ','')
            result.add(Game(pct[0], original_price[0], final_price[0], name[0], tag))

        return result
